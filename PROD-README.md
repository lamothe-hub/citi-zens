#Production Config Instructions...

##Environment Variables:
SERVER_PORT:8081
PROFILES:default
APP_LOG_LEVEL:INFO
SPRING_LOG_LEVEL:WARN
ROOT_LOG_LEVEL:WARN
DB_HOST:localhost
DB_PORT:3306
DB_SCHEMA:traderdb
DB_USER:root
DB_PASS:c0nygre
MQ_HOST:mqbroker
MQ_USER:citi
MQ_PASS:c0nygre

messaging.tradesender.database_poll_ms=5000
price.service.feed_poll_ms=15000
yahoo.price.feed.url=http://feed.conygre.com:8080/MockYahoo/quotes.csv
java.price.feed.url=http://localhost:8084

Contact jeffreylamothe@outlook.com if you have any problems...