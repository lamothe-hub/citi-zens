package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TradeTest.TradeState;
import com.citi.training.trader.model.TradeTest.TradeType;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.tradeController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class TradeControllerIT {
    public enum TradeType {
        BUY("true"), SELL("false");

        private final String xmlString;

        private TradeType(String xmlString) {
            this.xmlString = xmlString;
        }

        public String getXmlString() {
            return this.xmlString;
        }

        public static TradeType fromXmlString(String xmlValue) {
            switch (xmlValue.toLowerCase()) {
            case "true":
                return TradeType.BUY;
            case "false":
                return TradeType.SELL;
            default:
                throw new IllegalArgumentException(xmlValue);
            }
        }
    }

    public enum TradeState {
        INIT, WAITING_FOR_REPLY, FILLED, PARTIALLY_FILLED,
        CANCELED, DONE_FOR_DAY, REJECTED;
    }

    @XmlTransient
    private TradeType tradeType;

    @XmlTransient
    private TradeState state = TradeState.INIT;
    
    private static final Logger logger =
            LoggerFactory.getLogger(StockControllerIntegrationIT.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.trading.trader.rest.stock-base-path:/trade}")
    private String tradeBasePath;

    @Test
    public void findAll_returnsList() {
        Stock stock = new Stock(1, "AAPL");
        LocalDateTime date= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        Trade testTrade = new Trade(5, stock, 45.54, 100, TradeType.BUY.toString(), TradeState.INIT.toString(), date, simpleStrat);
       
        restTemplate.postForEntity(tradeBasePath,
                testTrade, Trade.class);

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                tradeBasePath,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }
    
    @Test
    public void findById_returnsCorrectId() {
        Stock stock = new Stock(1, "AAPL");
        LocalDateTime date= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        Trade testTrade = new Trade(-1, stock, 45.54, 100, TradeType.BUY.toString(), TradeState.INIT.toString(), date, simpleStrat);
       
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                        testTrade, Trade.class);

        //assertEquals(HttpStatus.CREATED, createdResponse.getStatusCode());

        Trade foundStock = restTemplate.getForObject(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                                Trade.class);

        assertEquals(foundStock.getId(), createdResponse.getBody().getId());
        assertEquals(foundStock.getId(), testTrade.getId());
    }
    
    @Test
    public void deleteById_deletes() {
        Stock stock = new Stock(1, "AAPL");
        LocalDateTime date= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        Trade testTrade = new Trade(-1, stock, 45.54, 100, TradeType.BUY.toString(), TradeState.INIT.toString(), date, simpleStrat);
       
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                        testTrade, Trade.class);

        Trade foundTrade = restTemplate.getForObject(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                Trade.class);

        logger.debug("Before delete, findById gives: " + foundTrade);
        assertNotNull(foundTrade);

        restTemplate.delete(tradeBasePath + "/" + createdResponse.getBody().getId());

        ResponseEntity<Trade> response = restTemplate.exchange(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                                HttpMethod.GET,
                                null,
                                Trade.class);

        logger.debug("After delete, findById response code is: " +
                     response.getStatusCode());
    }
}
