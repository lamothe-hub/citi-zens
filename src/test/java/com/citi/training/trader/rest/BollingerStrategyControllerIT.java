package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.Stock;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.StockController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
@Transactional
public class BollingerStrategyControllerIT {

    private static final Logger logger =
            LoggerFactory.getLogger(StockControllerIntegrationIT.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.trading.trader.rest.stock-base-path:/strategy}")
    private String strategyBasePath;
    
    @Test
    public void findAll_returnsList() {
        Stock stock = new Stock(1, "APPL");
        BollingerStrategy testStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        restTemplate.postForEntity(strategyBasePath,
                testStrategy, BollingerStrategy.class);

        ResponseEntity<List<BollingerStrategy>> findAllResponse = restTemplate.exchange(
                                strategyBasePath,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<BollingerStrategy>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(findAllResponse.getBody().get(0).getName(), testStrategy.getName());
    }  
    
    @Test
    public void findByName_returnsCorrectName() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        ResponseEntity<BollingerStrategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                        testStrategy, BollingerStrategy.class);

        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);

        BollingerStrategy foundStrategy = restTemplate.getForObject(
                                strategyBasePath + "/" + createdResponse.getBody().getName(),
                                BollingerStrategy.class);

        assertEquals(foundStrategy.getId(), createdResponse.getBody().getId());
        assertEquals(foundStrategy.getName(), testStrategy.getName());
    }
    @Test
    public void deleteByName_deletes() {
        Stock stock = new Stock(5, "AMZN");
        BollingerStrategy testStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        ResponseEntity<BollingerStrategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                        testStrategy, BollingerStrategy.class);

        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);

        BollingerStrategy foundStrategy = restTemplate.getForObject(
                                strategyBasePath + "/" + createdResponse.getBody().getName(),
                                BollingerStrategy.class);

        logger.debug("Before delete, findByName gives: " + foundStrategy);
        assertNotNull(foundStrategy);

        restTemplate.delete(strategyBasePath + "/" + createdResponse.getBody().getName());

        ResponseEntity<BollingerStrategy> response = restTemplate.exchange(
                                strategyBasePath + "/" + createdResponse.getBody().getName(),
                                HttpMethod.GET,
                                null,
                                BollingerStrategy.class);

        logger.debug("After delete, findByName response code is: " +
                     response.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    
    @Test
    public void stopStrategy_updatesStrategy() {
        Stock stock = new Stock(8, "GOOGL");
        BollingerStrategy testStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        ResponseEntity<BollingerStrategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                        testStrategy, BollingerStrategy.class);
        
        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
        BollingerStrategy foundStrategy = restTemplate.getForObject(
                strategyBasePath + "/stop/" + createdResponse.getBody().getName(),
                BollingerStrategy.class);
        
        assertEquals(foundStrategy.getStopped(), createdResponse.getBody().getStopped());
    }
    
    @Test
    public void startStrategy_updatesStrategy() {
        Stock stock = new Stock(8, "GOOGL");
        BollingerStrategy testStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        ResponseEntity<BollingerStrategy> createdResponse =
                restTemplate.postForEntity(strategyBasePath,
                        testStrategy, BollingerStrategy.class);
        
        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);
        BollingerStrategy foundStrategy = restTemplate.getForObject(
                strategyBasePath + "/start/" + createdResponse.getBody().getName(),
                BollingerStrategy.class);
        assertEquals(foundStrategy.getStopped(), createdResponse.getBody().getStopped());
    }
    
    
}
