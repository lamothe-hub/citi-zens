package com.citi.training.trader.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.Stock;

/**
 * Integration test for Stock REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.training.stockr.rest.AnalyticsController}.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class AnalyticsControllerIT {
    
    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.trading.trader.rest.stock-base-path:/analytics}")
    private String analyticsBasePath;
    
    @Test
    public void getAll_returnsList() {
        ChartDataPoint chartFound = restTemplate.getForObject(
                analyticsBasePath + "/testStrategy/5",
                ChartDataPoint.class);
        
         assertNotNull(chartFound);
    }
}
