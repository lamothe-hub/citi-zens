package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Test;

public class StockTest {

    @Test
    public void test_Stock_fullConstructor() {
        Stock testStock = new Stock (1, "AAPL");
        assert(testStock.getId() == 1);
        assert(testStock.getTicker() == "AAPL");
    }
    
    @Test
    public void test_Stock_setters() {
        Stock testStock = new Stock (1, "AAPL");
        testStock.setId(2);
        testStock.setTicker("MSFT");
        
        assert(testStock.getId() == 2);
        assert(testStock.getTicker() == "MSFT");
    }
    
    @Test
    public void test_Stock_toString() {
        Stock testStock = new Stock (1, "AAPL");

        assert(testStock.toString() != null);
        assert(testStock.toString().contains(Integer.toString(testStock.getId())));
        assert(testStock.toString().contains(testStock.getTicker()));
    }
    
}
