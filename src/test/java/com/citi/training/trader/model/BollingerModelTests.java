package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BollingerModelTests {

    private static final Logger logger = LoggerFactory.getLogger(BollingerModelTests.class);

    @Test
    public void test_BollingerStrategy_fullConstructor() {
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", 1, 200, 2.3, 1.5, 4, 0, 99.23, 2000, 1);
        assert (testBollStrat.getId() == 1);
        assert (testBollStrat.getName() == "test");
        assert (testBollStrat.getStock().getId() == 1);
        assert (testBollStrat.getSize() == 200);
        assert (testBollStrat.getVolMultipleHigh() == 2.3);
        assert (testBollStrat.getVolMultipleLow() == 1.5);
        assert (testBollStrat.getExitThreshold() == 4);
        assert (testBollStrat.getCurrentPosition() == 0);
        assert (testBollStrat.getLastTradePrice() == 99.23);
        assert (testBollStrat.getProfit() == 2000);
        assert (testBollStrat.getStopped() == 1);
        logger.debug(testBollStrat.toString());
    }

    @Test
    public void test_BollingerStrategy_withStockConstructor() {
        Stock stock = new Stock(2, "AAPL");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 0, 99.23, 2000, 1);
        assert (testBollStrat.getId() == 1);
        assert (testBollStrat.getName() == "test");
        assertEquals(testBollStrat.getStock(), stock);
        assert (testBollStrat.getSize() == 200);
        assert (testBollStrat.getVolMultipleHigh() == 2.3);
        assert (testBollStrat.getVolMultipleLow() == 1.5);
        assert (testBollStrat.getExitThreshold() == 4);
        assert (testBollStrat.getCurrentPosition() == 0);
        assert (testBollStrat.getLastTradePrice() == 99.23);
        assert (testBollStrat.getProfit() == 2000);
        assert (testBollStrat.getStopped() == 1);
        logger.debug(testBollStrat.toString());
    }

    @Test
    public void test_BollingerStrategy_noIdConstructor() {
        Stock stock = new Stock(2, "AAPL");
        BollingerStrategy testBollStrat = new BollingerStrategy("test", stock, 200, 2.3, 1.5, 4);
        assert (testBollStrat.getName() == "test");
        assertEquals(testBollStrat.getStock(), stock);
        assert (testBollStrat.getSize() == 200);
        assert (testBollStrat.getVolMultipleHigh() == 2.3);
        assert (testBollStrat.getVolMultipleLow() == 1.5);
        assert (testBollStrat.getExitThreshold() == 4);
        assert (testBollStrat.getCurrentPosition() == 0);
        assert (testBollStrat.getLastTradePrice() == -1);
        assert (testBollStrat.getProfit() == 0);
        assert (testBollStrat.getStopped() == 1);
        logger.debug(testBollStrat.toString());
    }

    @Test
    public void test_BollingerStrategy_setters() {
        Stock stock = new Stock(1, "AAPL");
        Stock newStock = new Stock(2, "GOOGL");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 0, 99.23, 2000, 1);
        testBollStrat.setId(2);
        ;
        testBollStrat.setStock(newStock);
        testBollStrat.setSize(300);
        testBollStrat.setVolMultipleHigh(1.5);
        testBollStrat.setVolMultipleLow(1.0);
        testBollStrat.setExitThreshold(7);
        testBollStrat.setCurrentPosition(1);
        testBollStrat.setName("TEST1");
        testBollStrat.setLastTradePrice(100);
        testBollStrat.setProfit(999);
        testBollStrat.setStopped(0);

        assert (testBollStrat.getId() == 2);
        assertEquals(testBollStrat.getStock(), newStock);
        assert (testBollStrat.getSize() == 300);
        assert (testBollStrat.getVolMultipleHigh() == 1.5);
        assert (testBollStrat.getVolMultipleLow() == 1.0);
        assert (testBollStrat.getExitThreshold() == 7);
        assert (testBollStrat.getCurrentPosition() == 1);
        assert (testBollStrat.getName() == "TEST1");
        assert (testBollStrat.getLastTradePrice() == 100);
        assert (testBollStrat.getProfit() == 999);
        assert (testBollStrat.getStopped() == 0);
    }

    @Test
    public void test_hasPosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 1, 99.23, 2000, 1);
        assert(testBollStrat.hasPosition());
    }
    
    @Test
    public void test_hasShortPosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, -9, 99.23, 2000, 1);
        assert(testBollStrat.hasShortPosition());
    }
    
    @Test
    public void test_hasLongPosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 9, 99.23, 2000, 1);
        assert(testBollStrat.hasLongPosition());
    }
    
    @Test
    public void test_takeShortPosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 9, 99.23, 2000, 1);
        testBollStrat.takeShortPosition();
        assert(testBollStrat.getCurrentPosition() == -1);
    }
    
    @Test
    public void test_takeLongPosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 9, 99.23, 2000, 1);
        testBollStrat.takeLongPosition();
        assert(testBollStrat.getCurrentPosition() == 1);
    }
    
    @Test
    public void test_closePosition() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 9, 99.23, 2000, 1);
        testBollStrat.closePosition();
        assert(testBollStrat.getCurrentPosition() == 0);
    }
    
    @Test
    public void test_addProfit() {
        Stock stock = new Stock(2, "MSFT");
        BollingerStrategy testBollStrat = new BollingerStrategy(1, "test", stock, 200, 2.3, 1.5, 4, 9, 99.23, 2000, 1);
        testBollStrat.addProfit(50);
        assert(testBollStrat.getProfit() == 2050);
    }


}
