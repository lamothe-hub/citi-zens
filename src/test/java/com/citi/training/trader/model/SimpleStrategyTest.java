package com.citi.training.trader.model;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2test")
@Transactional
public class SimpleStrategyTest {

    private static final Logger logger = LoggerFactory.getLogger(SimpleStrategyTest.class);

    @Test
    public void test_SimpleStrategy_fullConstructor() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 1, 99.56, 2000, date);
        assert (testSimpleStrat.getId() == 1);
        assert (testSimpleStrat.getName() == "test");
        assertEquals (testSimpleStrat.getStock(), stock);
        assert (testSimpleStrat.getSize() == 200);
        assert (testSimpleStrat.getExitProfitLoss() == 1000);
        assert (testSimpleStrat.getCurrentPosition() == 1);
        assert (testSimpleStrat.getLastTradePrice() == 99.56);
        assert (testSimpleStrat.getProfit() == 2000);
        assertEquals (testSimpleStrat.getStopped(), date);
        logger.debug(testSimpleStrat.toString());
    }
    
    @Test
    public void test_SimpleStrategy_notFullConstructor() {
        Stock stock = new Stock(2, "MSFT");
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", stock, 200);
        assert (testSimpleStrat.getId() == -1);
        assert (testSimpleStrat.getName() == "test");
        assertEquals (testSimpleStrat.getStock(), stock);
        assert (testSimpleStrat.getSize() == 200);
        assert (testSimpleStrat.getExitProfitLoss() == 20);
        assert (testSimpleStrat.getCurrentPosition() == 0);
        assert (testSimpleStrat.getLastTradePrice() == 0);
        assert (testSimpleStrat.getProfit() == 0);
        assertEquals (testSimpleStrat.getStopped(), null);
        logger.debug(testSimpleStrat.toString());
    }
    
    @Test
    public void test_SimpleStrategy_setters() {
        Stock stock = new Stock(1, "AAPL");
        Stock newStock = new Stock(2, "GOOGL");
        Date date = new Date();
        Date newDate = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 1, 99.56, 2000, date);
        testSimpleStrat.setId(2);
        testSimpleStrat.setStock(newStock);
        testSimpleStrat.setSize(300);
        testSimpleStrat.setExitProfitLoss(7000);
        testSimpleStrat.setCurrentPosition(5);
        testSimpleStrat.setName("TEST1");
        testSimpleStrat.setLastTradePrice(100);
        testSimpleStrat.setProfit(999);
        testSimpleStrat.setStopped(newDate);

        assert (testSimpleStrat.getId() == 2);
        assertEquals(testSimpleStrat.getStock(), newStock);
        assert (testSimpleStrat.getSize() == 300);
        assert (testSimpleStrat.getExitProfitLoss() == 7000);
        assert (testSimpleStrat.getCurrentPosition() == 5);
        assert (testSimpleStrat.getName() == "TEST1");
        assert (testSimpleStrat.getLastTradePrice() == 100);
        assert (testSimpleStrat.getProfit() == 999);
        assertEquals (testSimpleStrat.getStopped(),newDate);
    }
    
    @Test
    public void test_hasPosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 1, 99.56, 2000, date);
        assert(testSimpleStrat.hasPosition());
    }
    
    @Test
    public void test_hasShortPosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, -9, 99.56, 2000, date);
        assert(testSimpleStrat.hasShortPosition());
    }
    
    @Test
    public void test_hasLongPosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        assert(testSimpleStrat.hasLongPosition());
    }
    
    @Test
    public void test_takeShortPosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        testSimpleStrat.takeShortPosition();
        assert(testSimpleStrat.getCurrentPosition() == -1);
    }
    
    @Test
    public void test_takeLongPosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        testSimpleStrat.takeLongPosition();
        assert(testSimpleStrat.getCurrentPosition() == 1);
    }
    
    @Test
    public void test_closePosition() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        testSimpleStrat.closePosition();
        assert(testSimpleStrat.getCurrentPosition() == 0);
    }
    
    @Test
    public void test_addProfitLoss() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        testSimpleStrat.addProfitLoss(50);
        assert(testSimpleStrat.getProfit() == 2050);
        testSimpleStrat.addProfitLoss(3000);
        assertEquals(testSimpleStrat.getStopped(), date);
    }
    
    @Test
    public void test_stop() {
        Stock stock = new Stock(2, "MSFT");
        Date date = new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, date);
        testSimpleStrat.stop();
        assertEquals(testSimpleStrat.getStopped(), date);

    }
    
    @Test
    public void test_Price_toString() {
        Stock stock = new Stock (1, "AAPL");
        Date testDate= new Date();
        SimpleStrategy testSimpleStrat = new SimpleStrategy("test", 1, stock, 200, 1000, 9, 99.56, 2000, testDate);

        assert(testSimpleStrat.toString() != null);
        assert(testSimpleStrat.toString().contains(Double.toString(testSimpleStrat.getExitProfitLoss())));
        assert(testSimpleStrat.toString().contains(Integer.toString(testSimpleStrat.getId())));
        assert(testSimpleStrat.toString().contains(Integer.toString(testSimpleStrat.getSize())));
        assert(testSimpleStrat.toString().contains(Integer.toString(testSimpleStrat.getCurrentPosition())));
        assert(testSimpleStrat.toString().contains(stock.toString()));
        assert(testSimpleStrat.toString().contains(testDate.toString()));
        assert(testSimpleStrat.toString().contains(testSimpleStrat.getName()));
    }


    
}
