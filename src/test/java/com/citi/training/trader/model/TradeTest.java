package com.citi.training.trader.model;

import java.time.LocalDateTime;


import javax.xml.bind.annotation.XmlTransient;

import org.junit.Test;

public class TradeTest {

    public enum TradeType {
        BUY("true"), SELL("false");

        private final String xmlString;

        private TradeType(String xmlString) {
            this.xmlString = xmlString;
        }

        public String getXmlString() {
            return this.xmlString;
        }

        public static TradeType fromXmlString(String xmlValue) {
            switch (xmlValue.toLowerCase()) {
            case "true":
                return TradeType.BUY;
            case "false":
                return TradeType.SELL;
            default:
                throw new IllegalArgumentException(xmlValue);
            }
        }
    }

    public enum TradeState {
        INIT, WAITING_FOR_REPLY, FILLED, PARTIALLY_FILLED,
        CANCELED, DONE_FOR_DAY, REJECTED;
    }

    @XmlTransient
    private TradeType tradeType;

    @XmlTransient
    private TradeState state = TradeState.INIT;
    
    @Test
    public void test_Trade_fullConstructor() {
        Stock stock = new Stock(1, "AAPL");
        LocalDateTime date= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        Trade testTrade = new Trade(5, stock, 45.54, 100, TradeType.BUY.toString(), TradeState.INIT.toString(), date, simpleStrat);
        assert(testTrade.getId() == 5);
        assert(testTrade.getStock().equals(stock));
        assert(testTrade.getPrice() == 45.54);
        assert(testTrade.getSize() == 100);
        assert(testTrade.getTradeType() == testTrade.getTradeType());
        assert(testTrade.getState() == testTrade.getState());
        assert(testTrade.getStrategy().equals(simpleStrat));
        assert(testTrade.getLastStateChange().equals(date));
    }
    
    @Test
    public void test_Trade_setters() {
        Stock stock = new Stock(1, "AAPL");
        LocalDateTime date= LocalDateTime.now();
        Stock newStock = new Stock (2, "GOOGL");
        LocalDateTime newDate= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        SimpleStrategy newSimpleStrat = new SimpleStrategy("test1", newStock, 300);
        Trade testTrade = new Trade(5, stock, 45.54, 100, TradeType.BUY.toString(), TradeState.INIT.toString(), date, simpleStrat);
        testTrade.setId(7);
        testTrade.setStock(newStock);
        testTrade.setPrice(99.76);
        testTrade.setSize(346);
        testTrade.setTradeType(testTrade.getTradeType());
        testTrade.setState(testTrade.getState());
        testTrade.setStrategy(newSimpleStrat);
        testTrade.setLastStateChange(newDate);
        
        assert(testTrade.getId() == 7);
        assert(testTrade.getStock().equals(newStock));
        assert(testTrade.getPrice() == 99.76);
        assert(testTrade.getSize() == 346);
        assert(testTrade.getTradeType() == testTrade.getTradeType());
        assert(testTrade.getState() == testTrade.getState());
        assert(testTrade.getStrategy().equals(newSimpleStrat));
        assert(testTrade.getLastStateChange().equals(newDate));
    }

}
