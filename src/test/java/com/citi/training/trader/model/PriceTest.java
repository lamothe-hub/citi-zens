package com.citi.training.trader.model;
import java.util.Date;

import org.junit.Test;


public class PriceTest {

    @Test
    public void test_Price_fullConstructor() {
        Stock stock = new Stock (1, "AAPL");
        Date testDate= new Date();
        Price testPrice = new Price(0, stock, 9.99, testDate);
        assert(testPrice.getId() == 0);
        assert(testPrice.getStock().equals(stock));
        assert(testPrice.getPrice() == 9.99);
        assert(testPrice.getRecordedAt() == testDate);
    }
    
    @Test
    public void test_Price_StockPriceConstructor() {
        Stock stock = new Stock (1, "AAPL");
        Price testPrice = new Price(stock, 9.99);
        assert(testPrice.getId() == -1);
        assert(testPrice.getStock().equals(stock));
        assert(testPrice.getPrice() == 9.99);
    }
    
    @Test
    public void test_Price_StockPriceRecordedAtConstructor() {
        Stock stock = new Stock (1, "AAPL");
        Date testDate= new Date();
        Price testPrice = new Price(stock, 9.99, testDate);
        assert(testPrice.getId() == -1);
        assert(testPrice.getStock().equals(stock));
        assert(testPrice.getPrice() == 9.99);
        assert(testPrice.getRecordedAt() == testDate);
    }
    
    @Test
    public void test_Price_setters() {
        Stock stock = new Stock (1, "AAPL");
        Stock newStock = new Stock (2, "GOOGL");
        Date testDate= new Date();
        Date testNewDate= new Date();
        Price testPrice = new Price(0, stock, 9.99, testDate);
        testPrice.setId(1);
        testPrice.setStock(newStock);
        testPrice.setPrice(10.00);
        testPrice.setRecordedAt(testNewDate);
        
        assert(testPrice.getId() == 1);
        assert(testPrice.getStock() == newStock);
        assert(testPrice.getPrice() == 10.00);
        assert(testPrice.getRecordedAt() == testNewDate);
    }
    
    @Test
    public void test_Price_toString() {
        Stock stock = new Stock (1, "AAPL");
        Date testDate= new Date();
        Price testPrice = new Price(0, stock, 9.99, testDate);

        assert(testPrice.toString() != null);
        assert(testPrice.toString().contains(Double.toString(testPrice.getPrice())));
        assert(testPrice.toString().contains(Integer.toString(testPrice.getId())));
        assert(testPrice.toString().contains(stock.toString()));
        assert(testPrice.toString().contains(testDate.toString()));
    }
}
