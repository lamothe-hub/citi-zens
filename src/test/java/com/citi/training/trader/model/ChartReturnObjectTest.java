package com.citi.training.trader.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ChartReturnObjectTest {

    @Test
    public void test_ChartReturnObject_fullConstructor() {
        List <Double> testPrice = new ArrayList<Double>();
        testPrice.add(23.4);
        testPrice.add(89.56);
        
        List <Double> testAvg = new ArrayList<Double>();
        testAvg.add(56.2);
        testAvg.add(55.23);
        
        List <Double> testLow = new ArrayList<Double>();
        testLow.add(2.36);
        testLow.add(8.23);
        
        List <Double> testHigh = new ArrayList<Double>();
        testHigh.add(102.30);
        testHigh.add(256.77);
        
        List<String> testLabels = new ArrayList<String>();
        testLabels.add("Simple Trade");
        testLabels.add("Another Trade");
        
        ChartReturnObject testChartReturnObj = new ChartReturnObject(testPrice, testAvg, testLow, testHigh, testLabels);
        assertEquals(testChartReturnObj.getPrice(), testPrice);
        assertEquals(testChartReturnObj.getAvg(), testAvg);
        assertEquals(testChartReturnObj.getHigh(), testHigh);
        assertEquals(testChartReturnObj.getLow(), testLow);
        assertEquals(testChartReturnObj.getLabels(), testLabels);
    }
    
    @Test
    public void test_ChartReturnObject_setters() {
        List <Double> testPrice = new ArrayList<Double>();
        testPrice.add(23.4);
        testPrice.add(89.56);
        
        List <Double> testAvg = new ArrayList<Double>();
        testAvg.add(56.2);
        testAvg.add(55.23);
        
        List <Double> testLow = new ArrayList<Double>();
        testLow.add(2.36);
        testLow.add(8.23);
        
        List <Double> testHigh = new ArrayList<Double>();
        testHigh.add(102.30);
        testHigh.add(256.77);
        
        List<String> testLabels = new ArrayList<String>();
        testLabels.add("Simple Trade");
        testLabels.add("Another Trade");
        
        ChartReturnObject testChartReturnObj = new ChartReturnObject(testPrice, testAvg, testLow, testHigh, testLabels);
        
        List <Double> price = new ArrayList<Double>();
        price.add(56.88);
        price.add(66.65);
        
        List <Double> avg = new ArrayList<Double>();
        avg.add(59.90);
        avg.add(55.07);
        
        List <Double> low = new ArrayList<Double>();
        low.add(29.6);
        low.add(86.93);
        
        List <Double> high = new ArrayList<Double>();
        high.add(106.98);
        high.add(25.89);
        
        List<String> labels = new ArrayList<String>();
        labels.add("Test1");
        labels.add("Test2");
        
        testChartReturnObj.setPrice(price);
        testChartReturnObj.setAvg(avg);
        testChartReturnObj.setHigh(high);
        testChartReturnObj.setLow(low);
        testChartReturnObj.setLabels(labels);
        
        assertEquals(testChartReturnObj.getPrice(), price);
        assertEquals(testChartReturnObj.getAvg(), avg);
        assertEquals(testChartReturnObj.getHigh(), high);
        assertEquals(testChartReturnObj.getLow(), low);
        assertEquals(testChartReturnObj.getLabels(), labels);
    }
}
