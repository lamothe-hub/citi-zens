package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Test;

public class ChartDataPointTest {
    @Test
    public void test_ChartDataPoint_fullConstructor() {
        Date testDate= new Date();
        ChartDataPoint testChartDataPoint = new ChartDataPoint(0, 1, 23.5, 20.6, 25.7, 233.45, 100, testDate);
        assert(testChartDataPoint.getId() == 0);
        assert(testChartDataPoint.getStratId() == 1);
        assert(testChartDataPoint.getAvg() == 23.5);
        assert(testChartDataPoint.getLow() == 20.6);
        assert(testChartDataPoint.getHigh() == 25.7);
        assert(testChartDataPoint.getPrice() == 233.45);
        assert(testChartDataPoint.getMadeTrade() == 100);
        assert(testChartDataPoint.getTimeStamp() == testDate);
    }
    
    @Test
    public void test_ChartDataPoint_setters() {
        Date testDate= new Date();
        Date testNewDate= new Date();
        ChartDataPoint testChartDataPoint = new ChartDataPoint(0, 1, 23.5, 20.6, 25.7, 233.45, 100, testDate);
        testChartDataPoint.setId(2);
        testChartDataPoint.setStratId(8);
        testChartDataPoint.setAvg(54.32);
        testChartDataPoint.setLow(50.00);
        testChartDataPoint.setHigh(57.23);
        testChartDataPoint.setPrice(102.36);
        testChartDataPoint.setMadeTrade(33);
        testChartDataPoint.setTimeStamp(testNewDate);
        assert(testChartDataPoint.getId() == 2);
        assert(testChartDataPoint.getStratId() == 8);
        assert(testChartDataPoint.getAvg() == 54.32);
        assert(testChartDataPoint.getLow() == 50.00);
        assert(testChartDataPoint.getHigh() == 57.23);
        assert(testChartDataPoint.getPrice() == 102.36);
        assert(testChartDataPoint.getMadeTrade() == 33);
        assert(testChartDataPoint.getTimeStamp() == testNewDate);
    }

}
