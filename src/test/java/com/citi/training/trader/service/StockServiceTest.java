package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.Stock;



@RunWith(SpringRunner.class)
@SpringBootTest
public class StockServiceTest {

    @Autowired
    private StockService stockService;
    
    @MockBean
    private StockDao mockStockDao;
    
    @Test
    public void test_createRun() {
        int id = 3;
        Stock testStock = new Stock(87, "GOOGL");
        
        when(mockStockDao.create(any(Stock.class))).thenReturn(id);
        int createdId = stockService.create(testStock);
        
        verify(mockStockDao).create(testStock);
        assertEquals(id, createdId);
    }
    
    @Test
    public void test_deleteByIdRun() {
        stockService.deleteById(75);
        
        verify(mockStockDao).deleteById(75);
    }
    
    @Test
    public void test_findById() {
        int id = 3;
        Stock testStock = new Stock(id, "GOOGL");
        
        when(mockStockDao.findById(id)).thenReturn(testStock);
        
        Stock returnedStock = stockService.findById(id);
        
        verify(mockStockDao).findById(3);
        assertEquals(testStock, returnedStock);
    }
    
    @Test
    public void test_findByTicker() {
        String ticker = "GOOGL";
        Stock testStock = new Stock(3, ticker);
        
        when(mockStockDao.findByTicker(ticker)).thenReturn(testStock);
        
        Stock returnedStock = stockService.findByTicker(ticker);
        
        verify(mockStockDao).findByTicker(ticker);
        assertEquals(testStock, returnedStock);
    }
    
    @Test
    public void test_findAll() {
        List<Stock> testStockList = new ArrayList<Stock>();
        testStockList.add(new Stock (67, "MSFT"));
        
        when(mockStockDao.findAll()).thenReturn(testStockList);
        
        List<Stock> returnedStockList = stockService.findAll();
        assertEquals(testStockList, returnedStockList);
        
    }
}
