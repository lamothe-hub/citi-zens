package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.BollingerStrategyDao;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BollingerStrategyServiceTest {

    
    @Autowired
    private BollingerStrategyService bollingerStrategyService;
    
    @MockBean
    private BollingerStrategyDao mockBollingerStrategyDao;
    
    @Test
    public void test_createRun() {
        Stock stock = new Stock(87, "GOOGL");
        int id = 3;
        BollingerStrategy testBollingerStrategy = new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6);
        
        when(mockBollingerStrategyDao.save(any(BollingerStrategy.class))).thenReturn(id);
        int createdId = bollingerStrategyService.save(testBollingerStrategy);
        
        verify(mockBollingerStrategyDao).save(testBollingerStrategy);
        assertEquals(id, createdId);
    }
    
    @Test
    public void test_deleteByNameRun() {
        bollingerStrategyService.deleteByName("Test Strategy");
        
        verify(mockBollingerStrategyDao).deleteByName("Test Strategy");
    }
    
    @Test
    public void test_findByName() {
        Stock stock = new Stock(87, "GOOGL");

        String name = "Test Strategy";
        BollingerStrategy testBollingerStrategy = new BollingerStrategy(name, stock, 1, 100, 2.5, 1.6);
        
        when(mockBollingerStrategyDao.findByName(name)).thenReturn(testBollingerStrategy);
        
        BollingerStrategy returnedBollingerStrategy = bollingerStrategyService.findByName(name);
        
        verify(mockBollingerStrategyDao).findByName(name);
        assertEquals(testBollingerStrategy, returnedBollingerStrategy);
    }
    
    @Test
    public void test_findAll() {
        Stock stock = new Stock(87, "GOOGL");
        List<BollingerStrategy> testBollingerStrategyList = new ArrayList<BollingerStrategy>();
        testBollingerStrategyList.add(new BollingerStrategy ("Test Strategy", stock, 1, 100, 2.5, 1.6));
        
        when(mockBollingerStrategyDao.findAll()).thenReturn(testBollingerStrategyList);
        
        List<BollingerStrategy> returnedBollingerStrategyList = bollingerStrategyService.findAll();
        verify(mockBollingerStrategyDao).findAll();
        assertEquals(testBollingerStrategyList, returnedBollingerStrategyList);
        
    }
    
    @Test
    public void test_startStrategy() {
        Stock stock = new Stock(87, "GOOGL");

        String name = "Test Strategy";
        BollingerStrategy testBollingerStrategy = new BollingerStrategy(name, stock, 1, 100, 2.5, 1.6);
        
        when(mockBollingerStrategyDao.findByName(name)).thenReturn(testBollingerStrategy);
        
        //BollingerStrategy returnedBollingerStrategy = bollingerStrategyService.findByName(name);
        
        bollingerStrategyService.startStrategy(name);
        verify(mockBollingerStrategyDao).startStrategy(name);
        assert((mockBollingerStrategyDao).findByName(name).getStopped() == testBollingerStrategy.getStopped());
    }
    
    @Test
    public void test_stopStrategy() {
        Stock stock = new Stock(87, "GOOGL");

        String name = "Test Strategy";
        BollingerStrategy testBollingerStrategy = new BollingerStrategy(name, stock, 1, 100, 2.5, 1.6);
        
        when(mockBollingerStrategyDao.findByName(name)).thenReturn(testBollingerStrategy);
        
        //BollingerStrategy returnedBollingerStrategy = bollingerStrategyService.findByName(name);
        
        bollingerStrategyService.stopStrategy(name);
        verify(mockBollingerStrategyDao).stopStrategy(name);
        assert((mockBollingerStrategyDao).findByName(name).getStopped() == testBollingerStrategy.getStopped());
    }
    
}
