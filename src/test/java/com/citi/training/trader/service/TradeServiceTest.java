package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTest {

    @Autowired
    private TradeService tradeService;
    
    @MockBean
    private TradeDao mockTradeDao;
    
    @Test(expected=IllegalArgumentException.class)
    public void test_createRun() {
        int id = 3;
        Stock stock = new Stock(87, "GOOGL");
        LocalDateTime date= LocalDateTime.now();
        SimpleStrategy simpleStrat = new SimpleStrategy("test", stock, 200);
        Trade trade = new Trade(5, stock, 45.54, 100, "TRUE", "FILLED", date, simpleStrat);
        when(mockTradeDao.save(any(Trade.class))).thenReturn(id);
        int createdId = tradeService.save(trade);
        
        verify(mockTradeDao).save(trade);
        assertEquals(id, createdId);
    }
    
    @Test
    public void test_findAll() {
        List<Trade> testTradeList = new ArrayList<Trade>();
        testTradeList.add(new Trade());
        
        when(mockTradeDao.findAll()).thenReturn(testTradeList);
        
        List<Trade> returnedtradekList = tradeService.findAll();
        assertEquals(testTradeList, returnedtradekList);
    }
    
    @Test
    public void test_deleteById() {
        tradeService.deleteById(75);
        
        verify(mockTradeDao).deleteById(75);
    }
    
    @Test
    public void test_findById() {
        int id = 3;
        Trade testTrade = new Trade();
        
        when(mockTradeDao.findById(id)).thenReturn(testTrade);
        
        Trade returnedTrade = tradeService.findById(id);
        
        verify(mockTradeDao).findById(3);
        assertEquals(testTrade, returnedTrade);
    }
}
