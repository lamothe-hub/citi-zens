package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.ChartDataDao;
import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.ChartReturnObject;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChartDataServiceTest {

    @Autowired
    private ChartDataService chartDataService;
    
    @MockBean
    private ChartDataDao mockChartDataDao;
    
    @Test
    public void test_insertDataRun() {
        int id = 3;
        Date date = new Date();
        ChartDataPoint testChartDataPoint = new ChartDataPoint(1, 2, 45.67, 32.54, 57.12, 50.71, 0, date);
        
        when(mockChartDataDao.insertData(any(ChartDataPoint.class))).thenReturn(id);
        int createdId = chartDataService.insertData(testChartDataPoint);
        
        verify(mockChartDataDao).insertData(testChartDataPoint);
        assertEquals(id, createdId);
    }
    
    @Test
    public void test_getChartData() {
        Date date = new Date();
        List<ChartDataPoint> testChartDataPointList = new ArrayList<ChartDataPoint>();
        testChartDataPointList.add(new ChartDataPoint(1, 2, 45.67, 32.54, 57.12, 50.71, 0, date));
        
        when(mockChartDataDao.getChartData(6, 2)).thenReturn(testChartDataPointList);
        
        ChartReturnObject returnedChartDataPointList = chartDataService.getChartData("test",6);
        assertEquals(returnedChartDataPointList, returnedChartDataPointList);
        
    }
}
