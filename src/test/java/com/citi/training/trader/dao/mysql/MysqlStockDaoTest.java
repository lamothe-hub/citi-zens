package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStockDaoTest {

    @Autowired
    MysqlStockDao mysqlStockDao;
    
    @Test
    public void test_saveAndFindAll_works() {
        mysqlStockDao.create(new Stock(1, "MSFT"));
        assertThat(mysqlStockDao.findAll().size(), equalTo(1));
    }
    
    @Test
    public void test_saveAndFindByTicker_works() {
        mysqlStockDao.create(new Stock(1, "MSFT"));
        assertNotNull(mysqlStockDao.findByTicker("MSFT"));
    }
    
    @Test
    public void test_saveAndFindById_works() {
        Stock stock = new Stock (2, "AAPL");
        stock.setId(mysqlStockDao.create(stock));
        assertNotNull(mysqlStockDao.findById(stock.getId()));
    }
    
    @Test(expected=StockNotFoundException.class)
    public void test_saveAndDeleteById_works() {
        mysqlStockDao.create(new Stock(1, "MSFT"));
        mysqlStockDao.deleteById(1);
        mysqlStockDao.findById(1);
    }
}
