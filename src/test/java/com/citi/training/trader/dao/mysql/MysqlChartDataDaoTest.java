package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlChartDataDaoTest {
    
    @Autowired
    MysqlChartDataDao mysqlChartDataDao;
    
    @Test
    public void test_insertDataAndGetChartDate_works() {
        Date date = new Date();
        mysqlChartDataDao.insertData(new ChartDataPoint(1, 2, 45.67, 32.54, 57.12, 50.71, 0, date));
        assertThat(mysqlChartDataDao.getChartData(8,2).size(), equalTo(1));
    }
}
