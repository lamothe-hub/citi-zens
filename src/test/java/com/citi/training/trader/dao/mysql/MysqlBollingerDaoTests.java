package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StrategyNotFoundException;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlBollingerDaoTests {

    @Autowired
    MysqlBollingerStrategyDao mysqlBollingerStrategyDao;
    
    @Test
    public void test_saveAndFindAll_works() {
        Stock stock = new Stock(1, "APPL");
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        assertThat(mysqlBollingerStrategyDao.findAll().size(), equalTo(1));
    }
    
    @Test
    public void test_saveAndFindByName_works() {
        Stock stock = new Stock(1, "APPL");
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        assertNotNull(mysqlBollingerStrategyDao.findByName("Test Strategy"));
    }
    
    @Test(expected=StrategyNotFoundException.class)
    public void test_saveAndDeleteByName_works() {
        Stock stock = new Stock(1, "APPL");
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        mysqlBollingerStrategyDao.deleteByName("Test Strategy");
        mysqlBollingerStrategyDao.findByName("Test Strategy");
    }
    
    @Test
    public void test_startStrategy_works() {
        Stock stock = new Stock(1, "APPL");
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        mysqlBollingerStrategyDao.startStrategy("Test Strategy");
        assert(mysqlBollingerStrategyDao.findByName("Test Strategy").getStopped() == 0);
    }
    
    @Test
    public void test_stopStrategy_works() {
        Stock stock = new Stock(1, "APPL");
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        mysqlBollingerStrategyDao.stopStrategy("Test Strategy");
        assert(mysqlBollingerStrategyDao.findByName("Test Strategy").getStopped() == 1);
    }
    
    @Test(expected=StrategyNotFoundException.class)
    public void test_saveAndFindById_throwsNotFound() {
        Stock stock = new Stock(1, "APPL");        
        mysqlBollingerStrategyDao.save(new BollingerStrategy("Test Strategy", stock, 1, 100, 2.5, 1.6));
        mysqlBollingerStrategyDao.findByName("Not Existing Strategy");
    }

}
