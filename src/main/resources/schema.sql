
CREATE TABLE stock
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(8) NOT NULL,
  UNIQUE(`ticker`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `size` INT NOT NULL,
  `buy` BOOLEAN NOT NULL,
  `state` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `created` DATETIME NOT NULL,
  CONSTRAINT `trade_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `simple_strategy`
(
	`name` VARCHAR(30),
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `size` INT NOT NULL,
  `exit_profit_loss` DOUBLE NOT NULL,
  `current_position` INT DEFAULT 0,
  `last_trade_price` DOUBLE,
  `profit` DOUBLE,
  `stopped` DATETIME,
  CONSTRAINT `strategy_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE price
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE,
  `recorded_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `price_stock`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`)
);

create table `bollinger_strategy` 
(
	`id` INT NOT NULL AUTO_INCREMENT, 
    `name` VARCHAR(30),
	`stock_id` INT NOT NULL, 
    `ticker` VARCHAR(30),
	`size` INT NOT NULL, 
	`vol_multiple_high` DOUBLE, 
    `vol_multiple_low` DOUBLE,
	`exit_threshold` DOUBLE, 
	`current_position` INT DEFAULT 0,
	`last_trade_price` DOUBLE, 
	`profit` DOUBLE,
	`stopped` INT, 
	PRIMARY KEY (`id`)
);

CREATE TABLE chart_data
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `strat_id` INT, 
  `avg` DOUBLE,
  `low` DOUBLE,
  `high` DOUBLE,
  `price` DOUBLE,
  `made_trade` INT,
  `recorded_at` DATETIME,
  PRIMARY KEY(`id`)
);

