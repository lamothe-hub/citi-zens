package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

@Component
public class TradeSender {

    private final static Logger logger =
                    LoggerFactory.getLogger(TradeSender.class);

    @Autowired
    private TradeService tradeService;

    @Autowired
    private JmsTemplate jmstemplate;

    @Scheduled(fixedRateString = "${messaging.tradesender.database_poll_ms:15000}")
    public void getAndSendTrades() {
        for(Trade trade: tradeService.findAllByState(Trade.TradeState.INIT)) {
            sendTrade(trade);
            trade.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
            tradeService.save(trade);
        }
    }

    public void sendTrade(Trade tradeToSend) {
        logger.debug("Sending Trade " + tradeToSend);
        logger.debug("Trade XML:");
        logger.debug(Trade.toXml(tradeToSend));

        jmstemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
            message.setStringProperty("Operation", "update");
            message.setJMSCorrelationID(String.valueOf(tradeToSend.getId()));
            return message;
        });
    }

}
