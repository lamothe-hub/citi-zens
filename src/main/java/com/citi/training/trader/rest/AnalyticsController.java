package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.ChartReturnObject;
import com.citi.training.trader.service.ChartDataService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/analytics}")
public class AnalyticsController {
    
    private static final Logger logger =
            LoggerFactory.getLogger(AnalyticsController.class); 
    
    @Autowired
    ChartDataService chartDataService;
    
    @RequestMapping(value="/{strategyName}/{numEntries}",method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public ChartReturnObject getAll(@PathVariable String strategyName, @PathVariable int numEntries) {
        return chartDataService.getChartData(strategyName, numEntries);
    }
}
