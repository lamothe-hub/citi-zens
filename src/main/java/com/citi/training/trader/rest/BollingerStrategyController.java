package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.service.BollingerStrategyService;

/**
 * REST Controller for {@link com.citi.training.trader.model.BollingerStrategy} resource.
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/strategy}")
public class BollingerStrategyController {
    
    private static final Logger logger =
            LoggerFactory.getLogger(BollingerStrategyController.class);    

    @Autowired
    private BollingerStrategyService BollingerStrategyService;
    
    @RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    
    public List<BollingerStrategy> findAll() {
        logger.debug("findAll()");
        return BollingerStrategyService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<BollingerStrategy> create(@RequestBody BollingerStrategy bollingerStrategy) {
        logger.info("create(" + bollingerStrategy + ")");

        bollingerStrategy.setId(BollingerStrategyService.save(bollingerStrategy));
        logger.info("created strategy: " + bollingerStrategy);

        return new ResponseEntity<BollingerStrategy>(bollingerStrategy, HttpStatus.CREATED);
    }    
    
    @RequestMapping(value="/{name}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public BollingerStrategy findByName(@PathVariable String name) {
            logger.debug("findByName(" + name + ")");
            return BollingerStrategyService.findByName(name);
    }

    @RequestMapping(value="/{name}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String name) {
        logger.debug("deleteByName(" + name + ")");
        BollingerStrategyService.deleteByName(name);
    }    
    
    @RequestMapping(value="/start", method=RequestMethod.POST,
            produces=MediaType.APPLICATION_JSON_VALUE) 
    public void start(@RequestBody  String name) {
        logger.debug("startStrategy(" + name+")");
        BollingerStrategyService.startStrategy(name);
    }
    
    @RequestMapping(value="/stop", method=RequestMethod.POST,
            produces=MediaType.APPLICATION_JSON_VALUE) 
    public void stop(@RequestBody String name) {
        logger.debug("stopStrategy(" + name+")");
        BollingerStrategyService.stopStrategy(name);
    }
    
}
