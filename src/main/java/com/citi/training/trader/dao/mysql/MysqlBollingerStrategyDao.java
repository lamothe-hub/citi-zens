package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.BollingerStrategyDao;
import com.citi.training.trader.exceptions.StrategyNotFoundException;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.Stock;

/**
 * JDBC MySQL DAO implementation for bollinger_strategy table.
 *
 */
@Component
public class MysqlBollingerStrategyDao implements BollingerStrategyDao{

    private static final Logger logger =
                            LoggerFactory.getLogger(MysqlBollingerStrategyDao.class);

    private static String FIND_ALL_SQL = "select id as strategy_id, name, stock_id, ticker, size, " +
            "vol_multiple_high, vol_multiple_low, exit_threshold, current_position, last_trade_price, profit, stopped " +
            "from bollinger_strategy";

    private static String INSERT_SQL = "INSERT INTO bollinger_strategy (name, stock_id, ticker, size, " +
            "vol_multiple_high, vol_multiple_low, exit_threshold, current_position, last_trade_price, profit, stopped) " +
            "values (:name, :stock_id, :ticker, :size, " +
            ":vol_multiple_high, :vol_multiple_low, :exit_threshold, :current_position, :last_trade_price, :profit, :stopped)";
    
    private static String UPDATE_SQL ="UPDATE bollinger_strategy set name=:name, stock_id=:stock_id, ticker=:ticker, size=:size, " +
            "vol_multiple_high=:vol_multiple_high, vol_multiple_low=:vol_multiple_low, " +
            "exit_threshold=:exit_threshold, current_position=:current_position, last_trade_price=:last_trade_price, profit=:profit, stopped=:stopped where id=:id";

    private static String FIND_BY_NAME_SQL=FIND_ALL_SQL + " where name = ?";
    private static String DELETE_SQL = "delete from bollinger_strategy where name=?";
    private static String UPDATE_SQL_BY_NAME_STOP = "UPDATE bollinger_strategy SET stopped=1 where name=?";
    private static String UPDATE_SQL_BY_NAME_START= "UPDATE bollinger_strategy SET stopped=0 where name=?";


    
    @Autowired
    private JdbcTemplate tpl;
    
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
   
    public List<BollingerStrategy> findAll(){
        logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new BollingerStrategyMapper());
    }


    public int save(BollingerStrategy strategy) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("name", strategy.getName());
        namedParameters.addValue("stock_id", strategy.getStock().getId());
        namedParameters.addValue("ticker",strategy.getStock().getTicker());
        namedParameters.addValue("size", strategy.getSize());
        namedParameters.addValue("vol_multiple_high", strategy.getVolMultipleHigh());
        namedParameters.addValue("vol_multiple_low", strategy.getVolMultipleLow());
        namedParameters.addValue("exit_threshold", strategy.getExitThreshold());
        namedParameters.addValue("current_position", strategy.getCurrentPosition());
        namedParameters.addValue("last_trade_price", strategy.getLastTradePrice());
        namedParameters.addValue("profit", strategy.getProfit());
        namedParameters.addValue("stopped", strategy.getStopped());
        
        if(strategy.getId() <= 0) {
            logger.debug("Inserting bollingerStrateg: " + strategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            strategy.setId(keyHolder.getKey().intValue());
        } else {
            logger.debug("Updating simpleStrategy: " + strategy);
            namedParameters.addValue("id", strategy.getId());
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
        }

        logger.debug("Saved info: " + strategy);
        return strategy.getId();
    }
    
    @Override
    public BollingerStrategy findByName(String name) {
        logger.debug("findByName(" + name + ") SQL: [" + FIND_BY_NAME_SQL +"]");
        List<BollingerStrategy> bollingerStrategy = this.tpl.query(FIND_BY_NAME_SQL,
                new Object[]{name},
                new BollingerStrategyMapper()
        );
        if(bollingerStrategy.size() <= 0) {
            String warnMsg = "Requested Strategy not found, name: " + name;
            logger.warn(warnMsg);
            throw new StrategyNotFoundException(warnMsg);
        }
        if(bollingerStrategy.size() > 1) {
            logger.warn("Found more than one Strategy with id: " + name);
        }
        return bollingerStrategy.get(0);
    }
        

    
    private static final class BollingerStrategyMapper implements RowMapper<BollingerStrategy> {
        public BollingerStrategy mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping bollinger_strategy result set row num [" + rowNum + "], id : [" +
                         rs.getInt("strategy_id") + "]");
            
            return new BollingerStrategy(rs.getInt("strategy_id"),
            				 rs.getString("name"),
                             new Stock(rs.getInt("stock_id"), rs.getString("ticker")),
                             rs.getInt("size"),
                             rs.getDouble("vol_multiple_high"),
                             rs.getDouble("vol_multiple_low"),
                             rs.getDouble("exit_threshold"),
                             rs.getInt("current_position"),
                             rs.getDouble("last_trade_price"),
                             rs.getDouble("profit"),
                             rs.getInt("stopped"));
        }
    }
    
    @Override
    public void deleteByName(String name) {
    	
        logger.debug("deleteByName(" + name + ") SQL: [" + DELETE_SQL +"]");
    	BollingerStrategy strat = findByName(name);
    	int stockId = strat.getId();
    	
        if(this.tpl.update(DELETE_SQL, name) <= 0) {
            String warnMsg = "Failed to delete, strategy not found: " + name;
            logger.warn(warnMsg);
            throw new StrategyNotFoundException(warnMsg);
        }
        else {
        	
        	tpl.update("DELETE FROM trade WHERE stock_id = ?", stockId);
        	
            for(BollingerStrategy bollingerStrategy: findAll()) {
                logger.debug(bollingerStrategy.toString());
            }
        }
    }    
    
    @Override
    public void startStrategy(String name) {
        logger.debug("startStrategy (" + name + ") SQL: [" + UPDATE_SQL_BY_NAME_START +"]");
        
        
        if(findByName(name) == null) {
            String warnMsg = "Failed to update, strategy not found: " + name;
            logger.warn(warnMsg);
            throw new StrategyNotFoundException(warnMsg);
        }
        else {
            this.tpl.update(UPDATE_SQL_BY_NAME_START, name);
        }
    }
    
    @Override
    public void stopStrategy(String name) {
        logger.debug("startStrategy (" + name + ") SQL: [" + UPDATE_SQL_BY_NAME_STOP+"]");
        
        
        if(findByName(name) == null) {
            String warnMsg = "Failed to update, strategy not found: " + name;
            logger.warn(warnMsg);
            throw new StrategyNotFoundException(warnMsg);
        }
        else {
            this.tpl.update(UPDATE_SQL_BY_NAME_STOP, name);
        }
    }

}
