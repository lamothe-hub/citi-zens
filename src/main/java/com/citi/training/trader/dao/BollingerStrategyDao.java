package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.BollingerStrategy;

public interface BollingerStrategyDao {

    List<BollingerStrategy> findAll();

    int save(BollingerStrategy strategy);
    
    BollingerStrategy findByName(String name);
    
    void deleteByName(String name);
    
    void startStrategy(String name);
    
    void stopStrategy(String name);

}
