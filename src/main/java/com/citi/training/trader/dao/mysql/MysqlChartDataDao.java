package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.ChartDataDao;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.Stock;

@Component
public class MysqlChartDataDao implements ChartDataDao {
    private static final Logger logger =
            LoggerFactory.getLogger(MysqlChartDataDao.class);
    
    @Autowired
    private JdbcTemplate tpl;
    
    private static String FIND_ALL_SQL = "select id, strat_id, avg, low, high, price, made_trade, recorded_at from chart_data " + 
    		"where strat_id=? order by id desc limit ?";

    
    public int insertData(ChartDataPoint cd) {
    	return tpl.update("INSERT INTO chart_data (strat_id, avg, low, high, price, made_trade, recorded_at) " +
    			"VALUES (?, ?, ?, ?, ?, ?, ?)", cd.getStratId(), cd.getAvg(), cd.getLow(), cd.getHigh(), 
    			cd.getPrice(), cd.getMadeTrade(), cd.getTimeStamp());
    }
    
    public List<ChartDataPoint> getChartData(int numEntries, int strategyId) {
    	return tpl.query(FIND_ALL_SQL, new ChartDataMapper(), strategyId, numEntries);
    }
    
    private static final class ChartDataMapper implements RowMapper<ChartDataPoint> {
        public ChartDataPoint mapRow(ResultSet rs, int rowNum) throws SQLException {
            
            return new ChartDataPoint(rs.getInt("id"),
            				 rs.getInt("strat_id"),
            				 rs.getDouble("avg"),
                             rs.getDouble("low"),
                             rs.getDouble("high"),
                             rs.getDouble("price"),
                             rs.getInt("made_trade"),
                             rs.getDate("recorded_at"));
        }
    }
}
