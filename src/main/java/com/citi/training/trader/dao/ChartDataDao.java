package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.ChartDataPoint;

public interface ChartDataDao {
	
	public int insertData(ChartDataPoint chartData);
	public List<ChartDataPoint> getChartData(int numEntries, int strategyId);
	
}
