package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.BollingerStrategyDao;
import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.ChartDataService;

@Component
public class BollingerStrategyAlgorithm implements StrategyAlgorithm {
	
	private double rollingAverage;
	private boolean alreadyLogged = false;
	private static int TIME_INTERVAL = 10;
    private static final Logger logger =
            LoggerFactory.getLogger(BollingerStrategyAlgorithm.class);

	@Autowired
	private PriceDao priceDao;
	
	@Autowired
	private TradeSender tradeSender;
	
	@Autowired
	private BollingerStrategyDao strategyDao;
	
	@Autowired
	private ChartDataService chartDataService;
	
	@Scheduled(fixedRateString = "${bollinger.strategy.refresh.rate_ms:15000}")
	public void run() {
		for(BollingerStrategy strategy: strategyDao.findAll()) {
			
			List<Price> prices = priceDao.findLatest(strategy.getStock(), TIME_INTERVAL);
			rollingAverage = calculateRollingAverage(prices);
			double stdDev = calculateStdDev(prices);
			double mostRecentPrice = 0;
			if(prices.size() > 1) mostRecentPrice = prices.get(0).getPrice();
			
			// Enter data into chart table each iteration...
			double lowerLim = rollingAverage + (-1 * strategy.getVolMultipleLow() * stdDev);
			double upperLim = rollingAverage + (strategy.getVolMultipleHigh() * stdDev);
			ChartDataPoint currChartData = new ChartDataPoint(-1, strategy.getId(), 
								rollingAverage, lowerLim, upperLim, mostRecentPrice, 0, new Date());
			int returnStatus = chartDataService.insertData(currChartData); 
			
			
			if(strategy.getStopped() == 1) {
                continue; // this strategy is stopped! Exit loop.
            }
			
			if(!strategy.hasPosition()) {

				logger.debug("Number of prices: " + prices.size());
				if(prices.size() < TIME_INTERVAL) {
                    logger.warn("Unable to execute strategy, not enough price data: " + strategy); 
                    continue;
                }
			
				logger.debug("Strategy: " + strategy.getStock().getTicker() + "; Rolling Average: " + this.rollingAverage + "; Std Dev: " + stdDev + "; Previous price: " + prices.get(prices.size() -1).getPrice()); 
				
				if(mostRecentPrice > this.rollingAverage + (strategy.getVolMultipleHigh() * stdDev )) {
					
					// We have breached the upper volatility threshold - open a short position.					
					logger.info("Taking short position for strategy: " + strategy.getName());
                    strategy.takeShortPosition();
                    strategy.setLastTradePrice(makeTrade(BollingerToSimple(strategy), Trade.TradeType.SELL));
					
				} else if(mostRecentPrice < this.rollingAverage - (strategy.getVolMultipleLow() * stdDev)) {
					
					// We have breached the bottom volatility threshold - open a long position.
					logger.info("Taking long position for strategy: " + strategy.getName());
                    strategy.takeLongPosition();
                    strategy.setLastTradePrice(makeTrade(BollingerToSimple(strategy), Trade.TradeType.BUY));
                    
				} else {
				
	                    logger.debug("Neither band breached: taking no action");
	                    continue;
	                    
				}
			} else if(strategy.hasLongPosition() & mostRecentPrice > (strategy.getLastTradePrice() + (strategy.getLastTradePrice() * strategy.getExitThreshold() * .01))) {
				
                logger.info("Closing long position for strategy: " + strategy.getName());
                double thisTradePrice = makeTrade(BollingerToSimple(strategy), Trade.TradeType.SELL);
                logger.info("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                        thisTradePrice);
                closePosition((thisTradePrice - strategy.getLastTradePrice()) * strategy.getSize(), strategy);
			
			} else if (strategy.hasShortPosition() & mostRecentPrice < strategy.getLastTradePrice() - (strategy.getLastTradePrice() * strategy.getExitThreshold() * .01)) {
				
				logger.info("Closing short position for strategy: " + strategy.getName());
                double thisTradePrice = makeTrade(BollingerToSimple(strategy), Trade.TradeType.BUY);
                logger.info("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                             thisTradePrice);
                closePosition((strategy.getLastTradePrice() - thisTradePrice) * strategy.getSize(), strategy);
                
			}
			strategyDao.save(strategy);
		}
	}
	
	
	private void logStatus(BollingerStrategy strategy, double rollingAverage, double mostRecentPrice) {
		
		double lowerLim = strategy.getLastTradePrice() + (-1 * strategy.getLastTradePrice() * strategy.getExitThreshold() * .01);
		double upperLim = strategy.getLastTradePrice() + (strategy.getLastTradePrice() * strategy.getExitThreshold() * .01);
		
		logger.debug("");
		logger.debug(strategy.getName() + ": " + strategy.toString());
		logger.debug(" - Position: " + strategy.getCurrentPosition());
		logger.debug(" - TradePrice: " + strategy.getLastTradePrice());
		logger.debug(" - Lower : RollAvg : Upper: " + lowerLim + ": " + rollingAverage + ": " + upperLim);
		logger.debug(" - CurrentPrice: " + mostRecentPrice);
		logger.debug("");

	}
	
	private void closePosition(double profit, BollingerStrategy strategy) {
        logger.debug("Recording profit/loss of: " + profit +
                     " for strategy: " + strategy);
        strategy.addProfit(profit);
        strategy.closePosition();
    }
	
	private SimpleStrategy BollingerToSimple(BollingerStrategy bs) {
		return new SimpleStrategy(
				bs.getName(), bs.getId(), bs.getStock(), bs.getSize(), -1, 
				bs.getCurrentPosition(), bs.getLastTradePrice(), 
				bs.getProfit(), null);
	}
	
    private double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(strategy.getStock(), currentPrice.getPrice(),
                                        strategy.getSize(), tradeType,
                                        strategy));
        return currentPrice.getPrice();
    }

	
	private double calculateRollingAverage(List<Price> prices) {

		double sum = 0;
		for(Price price: prices) {
			sum += price.getPrice();
		}
		return sum / prices.size();
		
	}
	
	private double calculateStdDev(List<Price> prices) {
		
		double[] rawPriceArray = new double[prices.size()];
		for(int i = 0; i < prices.size(); i++) {
			rawPriceArray[i] = prices.get(i).getPrice();
		}
		
		double sum = 0.0, standardDeviation = 0.0;
        int length = rawPriceArray.length;
        for(double num : rawPriceArray) {
            sum += num;
        }
        double mean = sum/length;
        for(double num: rawPriceArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        return Math.sqrt(standardDeviation/length);
	}
}
