package com.citi.training.trader.model;
import java.util.List;

public class ChartReturnObject {
	private List<Double> price;
	private List<Double> avg;
	private List<Double> high;
	private List<Double> low;
	private List<String> labels;
	
	public ChartReturnObject() {}
	
	public ChartReturnObject(List<Double> price, List<Double> avg, List<Double> low,  List<Double> high, List<String> labels) {
		this.price = price;
		this.avg = avg;
		this.low = low;
		this.high = high;
		this.labels = labels;
	}
	public List<Double> getPrice() {
		return price;
	}
	public void setPrice(List<Double> price) {
		this.price = price;
	}
	public List<Double> getAvg() {
		return avg;
	}
	public void setAvg(List<Double> avg) {
		this.avg = avg;
	}
	public List<Double> getHigh() {
		return high;
	}
	public void setHigh(List<Double> high) {
		this.high = high;
	}
	public List<Double> getLow() {
		return low;
	}
	public void setLow(List<Double> low) {
		this.low = low;
	}
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	
}
