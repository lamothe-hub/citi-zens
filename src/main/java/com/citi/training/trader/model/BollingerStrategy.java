package com.citi.training.trader.model;

/**
 * Implements Bollinger Bands Algorithm
 * for Automated Trading System
 *
 * 
 */
public class BollingerStrategy {
	
    private int id;
    String name;
    private Stock stock;
    private int size;
    private double volMultipleHigh;
    private double volMultipleLow;
    private double exitThreshold;
    private int currentPosition;
    private double lastTradePrice;
    private double profit;
    private int stopped = 1; // Default to null
    
    /**
     * Default Constructor
     */
    public BollingerStrategy() {}
    
    /**
     * Full constructor
     * @param id This Strategy's id
     * @param name This Strategy's name
     * @param stock_id This Strategy's stock id
     * @param size This Strategy's size. The size of the stock to trade
     * @param volMultipleHigh This Strategy's high multiple of standard deviation
     * @param volMultipleLow This Strategy's low multiple of standard deviation
     * @param exitThreshold This Strategy's percentage of exit/loss
     * @param currentPosition This Strategy's current position
     * @param lastTradePrice This Strategy's last trade price
     * @param profit This Strategy's profit
     * @param stopped This Strategy's stopped value (zero if running, one if stopped)
     */
    public BollingerStrategy(int id, String name, int stock_id, int size, 
			double volMultipleHigh, double volMultipleLow, double exitThreshold, 
			int currentPosition, double lastTradePrice, 
			double profit, int stopped) {
		this.id = id; 
		this.name = name;
		this.stock = new Stock(stock_id, "UNKNOWN");
		this.size = size;
		this.volMultipleHigh = volMultipleHigh;
		this.volMultipleLow = volMultipleLow;
		this.exitThreshold = exitThreshold;
		this.currentPosition = currentPosition;
		this.lastTradePrice = lastTradePrice;
		this.profit = profit; 
		this.stopped = stopped;
}
    /**
     * Constructor with a stock as a parameter
     * @param id This Strategy's id
     * @param name This Strategy's name
     * @param stock This Strategy's stock
     * @param size This Strategy's size. The size of the stock to trade
     * @param volMultipleHigh This Strategy's high multiple of standard deviation
     * @param volMultipleLow This Strategy's low multiple of standard deviation
     * @param exitThreshold This Strategy's percentage of exit/loss
     * @param currentPosition This Strategy's current position
     * @param lastTradePrice This Strategy's last trade price
     * @param profit This Strategy's profit
     * @param stopped This Strategy's stopped value (zero if running, one if stopped)
     * 
     */
    public BollingerStrategy(int id, String name, Stock stock, int size, 
			double volMultipleHigh, double volMultipleLow, double exitThreshold, 
			int currentPosition, double lastTradePrice, 
			double profit, int stopped) {
    	this.id = id; 
    	this.name = name;
    	this.stock = stock;
    	
    	this.size = size;
		this.volMultipleHigh = volMultipleHigh;
		this.volMultipleLow = volMultipleLow;
		this.exitThreshold = exitThreshold;
		this.currentPosition = currentPosition;
		this.lastTradePrice = lastTradePrice;
		this.profit = profit; 
		this.stopped = stopped;
    }
    
    
    /**
     * Constructor for DAO access
     * @param name This Strategy's name
     * @param stock This Strategy's stock
     * @param size This Strategy's size. The size of the stock to trade
     * @param volMultipleHigh This Strategy's high multiple of standard deviation
     * @param volMultipleLow This Strategy's low multiple of standard deviation
     * @param exitThreshold This Strategy's percentage of exit/loss
     */
    
    public BollingerStrategy(String name, Stock stock, int size, double volMultipleHigh, 
    						double volMultipleLow, double exitThreshold) {
    	this.name = name; 
    	this.stock = stock;
    	this.size = size;
    	this.volMultipleHigh = volMultipleHigh;
    	this.volMultipleLow = volMultipleLow;
    	this.exitThreshold = exitThreshold;
    	this.currentPosition = 0;
    	this.lastTradePrice = -1;
    	this.profit = 0;
    	this.stopped = 1;
    }
    
    
    /**
     * Gets the id of this Bollinger Strategy
     * @return this Strategy's id
     */
	public int getId() {
		return id;
	}

	/**
	 * Changes the id of this Bollinger Strategy.
	 * Every Strategy will get randomly assigned id.
	 * @param id This Strategy's new id.
	 */
	public void setId(int id) {
		this.id = id;
	}

    /**
     * Gets the stock of this Bollinger Strategy
     * @return this Strategy's stock
     */
	public Stock getStock() {
		return stock;
	}

	   /**
     * Changes the stock traded of this Bollinger Strategy.
     * Every Strategy must have a stock to trade.
     * @param stock This Strategy's new stock.
     */
	public void setStock(Stock stock) {
		this.stock = stock;
	}

    /**
     * Gets the size of this Bollinger Strategy
     * The size is the number of shares to trade
     * @return this Strategy's size
     */
	public int getSize() {
		return size;
	}

	   /**
     * Changes the size of this Bollinger Strategy.
     * The size is the number of shares to trade
     * @param size This Strategy's new size.
     */
	public void setSize(int size) {
		this.size = size;
	}

    /**
     * Gets the high multiple of standard deviation of this Bollinger Strategy
     * @return this Strategy's high multiple of standard deviation
     */
	public double getVolMultipleHigh() {
		return volMultipleHigh;
	}

    /**
   * Changes the high multiple of standard deviation of this Bollinger Strategy.
   * @param volMultiple This Strategy's new high multiple.
   */
	public void setVolMultipleHigh(double volMultiple) {
		this.volMultipleHigh = volMultiple;
	}

    /**
     * Gets the exit loss/profit percentage of this Bollinger Strategy
     * @return this Strategy's the exit loss/profit percentage
     */
	public double getExitThreshold() {
		return exitThreshold;
	}

    /**
   * Changes the value for exit loss/profit percentage of this Bollinger Strategy.
   * This value is percentage (decimal)
   * @param exitThreshold This Strategy's new exitThreshold.
   */
	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

    /**
     * Gets the current position of this Bollinger Strategy
     * @return this Strategy's current position
     */
	public int getCurrentPosition() {
		return currentPosition;
	}

    /**
   * Changes current position of this Bollinger Strategy.
   * @param currentPosition This Strategy's new current position.
   */
	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

    /**
     * Gets the name of this Bollinger Strategy
     * @return this Strategy's name
     */
	public String getName() {
		return name;
	}

    /**
   * Changes the name of this Bollinger Strategy.
   * @param name This Strategy's new name.
   */
	public void setName(String name) {
		this.name = name;
	}

    /**
     * Gets the price of the stock from the last trade of this Bollinger Strategy
     * @return this Strategy's last trade price
     */
	public double getLastTradePrice() {
		return lastTradePrice;
	}

    /**
   * Changes the price of the stock from the last trade of this Bollinger Strategy.
   * @param lastTradePrice This Strategy's new last trade price.
   */
	public void setLastTradePrice(double lastTradePrice) {
		this.lastTradePrice = lastTradePrice;
	}

    /**
     * Gets the profit or loss of this Bollinger Strategy
     * after the position is closed
     * @return this Strategy's profit or loss
     */
	public double getProfit() {
		return profit;
	}

    /**
   * Changes the profit of this Bollinger Strategy.
   * @param profit This Strategy's new profit.
   */
	public void setProfit(double profit) {
		this.profit = profit;
	}
	
    /**
     * Gets the stop value this Bollinger Strategy
     * if the stop value is zero, the strategy is running
     * if the stop value is one, the strategy is stopped
     * @return this Strategy's stop value
     */
	public int getStopped() {
		return stopped;
	}

    /**
   * Changes the stop value of this Bollinger Strategy.
   * @param stopped This Strategy's new stopped value (zero or one).
   */
	public void setStopped(int stopped) {
		this.stopped = stopped;
	}
	
	/**
	 * Changes stop value to one
	 * When stop value is one, this strategy is not running
	 */
	public void stop() {
		this.stopped = 1;
	}
	
	/**
	 * Opens the position
	 * @return true if current position is not zero
	 */
    public boolean hasPosition() {
        return this.currentPosition != 0;
    }

    /**
     * Checks if the position is short (sell)
     * @return true if current position less than zero
     */
    public boolean hasShortPosition() {
        return this.currentPosition < 0;
    }

    /**
     * Checks if the position is long (buy)
     * @return true if current position greater than zero
     */
    public boolean hasLongPosition() {
        return this.currentPosition > 0;
    }

    /**
     * Opens short position (sells)
     * Sets current position to -1
     */
    public void takeShortPosition() {
        this.currentPosition = -1;
    }

    /**
     * Opens long position (buys)
     * Sets current position to 1
     */
    public void takeLongPosition() {
        this.currentPosition = 1;
    }

    /**
     * Closes  position (buy/sell)
     * Sets current position to 0
     */
    public void closePosition() {
        this.currentPosition = 0;
    }
    
    /**
     * Gets the low multiple of standard deviation of this Bollinger Strategy
     * @return this Strategy's low multiple of standard deviation
     */
	public double getVolMultipleLow() {
		return volMultipleLow;
	}

    /**
   * Changes the low multiple of standard deviation of this Bollinger Strategy.
   * @param volMultipleLow This Strategy's new low multiple.
   */
	public void setVolMultipleLow(double volMultipleLow) {
		this.volMultipleLow = volMultipleLow;
	}
	
	/**
	 * Adds profit to existing profit
	 * @param profit This Strategy's new profit
	 */
	 public void addProfit(double profit) {
	        this.profit += profit;
	 }

   
    



    
}
