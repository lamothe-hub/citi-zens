package com.citi.training.trader.model;

import java.util.Date;

public class ChartDataPoint {
	private int id;
	private int stratId;
	private double avg;
	private double low;
	private double high;
	private double price;
	private int madeTrade;
	private Date recordedAt;
	
	public ChartDataPoint() {}
	
	public ChartDataPoint(int id, int stratId, double avg, double low, double high, double price, int madeTrade, Date timeStamp) {
		this.id = id; 
		this.stratId = stratId;
		this.avg = avg; 
		this.low = low;
		this.high = high;
		this.price = price;
		this.madeTrade = madeTrade;
		this.recordedAt = timeStamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public int getMadeTrade() {
		return madeTrade;
	}

	public void setMadeTrade(int madeTrade) {
		this.madeTrade = madeTrade;
	}
	public Date getTimeStamp() {
		return recordedAt;
	}

	public void setTimeStamp(Date timeStamp) {
		this.recordedAt = timeStamp;
	}
	public int getStratId() {
		return stratId;
	}

	public void setStratId(int stratId) {
		this.stratId = stratId;
	}
}
