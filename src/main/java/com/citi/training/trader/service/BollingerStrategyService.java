package com.citi.training.trader.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.dao.BollingerStrategyDao;

@Component
public class BollingerStrategyService {
    
    private static final Logger logger =
            LoggerFactory.getLogger(BollingerStrategyService.class);
    @Autowired
    private BollingerStrategyDao bollingerStrategyDao;

    @Autowired
    private StockDao stockService;    

    public int save(BollingerStrategy BollingerStrategy) {
        //make sure stock id is filled in the trade object
        if(BollingerStrategy.getStock() == null) {
            logger.error("Attempt to save bollinger strategy with no stock id: " + BollingerStrategy);
            throw new IllegalArgumentException(
                    "Unable to save bollinger strategy with no stock reference");
        }
        if(BollingerStrategy.getStock().getId() <= 0) {
            BollingerStrategy.setStock(stockService.findByTicker(BollingerStrategy.getStock().getTicker()));
        }        
        logger.info("Saving Strategy");
        return bollingerStrategyDao.save(BollingerStrategy);
    }    

    public List<BollingerStrategy> findAll() {
        return bollingerStrategyDao.findAll();
    }    
    
    public BollingerStrategy findByName(String name) {
        return bollingerStrategyDao.findByName(name);
    }
    
    public void deleteByName(String name) {
        bollingerStrategyDao.deleteByName(name);
    }

    public void startStrategy(String name) {
        bollingerStrategyDao.startStrategy(name);
    }
    
    public void stopStrategy(String name) {
        bollingerStrategyDao.stopStrategy(name);
    }
}
