package com.citi.training.trader.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.BollingerStrategyDao;
import com.citi.training.trader.dao.ChartDataDao;
import com.citi.training.trader.model.BollingerStrategy;
import com.citi.training.trader.model.ChartDataPoint;
import com.citi.training.trader.model.ChartReturnObject; 

@Component	
public class ChartDataService {
	
    private static final Logger logger =
            LoggerFactory.getLogger(ChartDataService.class);  
    
	@Autowired
	ChartDataDao chartDataDao; 
	
	@Autowired
	BollingerStrategyDao bollingerStrategyDao;
	
	public int insertData(ChartDataPoint chartData) {
		return chartDataDao.insertData(chartData);
	}
	
	public ChartReturnObject getChartData(String stratName, int numEntries) {
		// Step 1: get the id from the name
		BollingerStrategy currStrat = bollingerStrategyDao.findByName(stratName);
		int stratId = currStrat.getId(); 
		logger.debug("Retrieved strat id of: " + stratId);
		
		// Step 2: get the list of ChartDataPoint objects associated with that id:
		
		// Most recent data point is FIRST in list. May have to reverse order.
		List<ChartDataPoint> chartDataPointList = chartDataDao.getChartData(numEntries, stratId);
		List<Double> price = new ArrayList<Double>(); 
		List<Double> avg = new ArrayList<Double>(); 
		List<Double> high = new ArrayList<Double>();
		List<Double> low = new ArrayList<Double>(); 
		List<String> labels = new ArrayList<String>();
		
		int i = 0;

		logger.debug("About to start the ChartDataPoint loop");
		for(ChartDataPoint dp: chartDataPointList) {
			price.add(dp.getPrice());
			avg.add(dp.getAvg()); 
			high.add(dp.getHigh()); 
			low.add(dp.getLow()); 
			labels.add(Integer.toString(i));
			i++;
		} 
		logger.debug("Completed the loop.");
		return new ChartReturnObject(price, avg, low, high, labels);
	}

}
